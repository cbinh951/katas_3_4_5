<?php

class StringCaculator
{
    const MAX_NUMBER_ALLOWED = 1000;

    function add($numbers){
        $numbers = $this->parseNumbers($numbers);
        $solution = 0;

        foreach($numbers as $number){
            $this->guardAgainstInvalidNumber($number);
            if($number >= self::MAX_NUMBER_ALLOWED) continue;
            $solution += (int)$number;
        }
        
        return $solution;
    }

    private function guardAgainstInvalidNumber($number){
        if($number < 0) throw new InvalidArgumentException;
    }

    private function parseNumbers($numbers){
        return preg_split('/\s*(,|\\\n)\s*/', $numbers);
    }
}
